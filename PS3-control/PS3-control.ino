/*
 Example sketch for the PS3 USB library - developed by Kristian Lauszus
 For more information visit my blog: http://blog.tkjelectronics.dk/ or
 send me an e-mail:  kristianl@tkjelectronics.com
 */

#include <PS3USB.h>

#define START_FLAG ((uint8_t) '$')
#define STOP_FLAG ((uint8_t) '@')
#define DRONE_ADDR 0
#define LAPTOP_ADDR 1
#define CONTROLLER_ADDR 2


#define CONTRL_THROTTLE 20
#define CONTRL_YAW 21
#define CONTRL_PITCH 22
#define CONTRL_ROLL 23

USB Usb;
/* You can create the instance of the class in two ways */
PS3USB PS3(&Usb); // This will just create the instance

uint8_t state = 200;

void setup() {
  Serial.begin(115200);

  if (Usb.Init() == -1) {
    Serial.print(F("\r\nOSC did not start"));
    while (1); //halt
  }
  Serial.print(F("\r\nPS3 USB Library Started"));
}
void loop() {
    Usb.Task();

    if (PS3.PS3Connected || PS3.PS3NavigationConnected) {
      sendFrameB(CONTRL_YAW, PS3.getAnalogHat(LeftHatX));    
      sendFrameB(CONTRL_THROTTLE, PS3.getAnalogHat(LeftHatY));
      sendFrameB(CONTRL_ROLL, PS3.getAnalogHat(RightHatX));
      sendFrameB(CONTRL_PITCH, PS3.getAnalogHat(RightHatY));
    }
//    // Analog button values can be read from almost all buttons
//    if (PS3.getAnalogButton(L2) || PS3.getAnalogButton(R2)) {
//      Serial.print(F("\r\nL2: "));
//      Serial.print(PS3.getAnalogButton(L2));
//    if (!PS3.PS3NavigationConnected) {
//      Serial.print(F("\tR2: "));
//      Serial.print(PS3.getAnalogButton(R2));
//      }
//    }
//    if (PS3.getButtonClick(PS))
//      Serial.print(F("\r\nPS"));
//    if (PS3.getButtonClick(TRIANGLE))
//      Serial.print(F("\r\nTraingle"));
//    if (PS3.getButtonClick(CIRCLE))
//      Serial.print(F("\r\nCircle"));
//    if (PS3.getButtonClick(CROSS))
//      Serial.print(F("\r\nCross"));
//    if (PS3.getButtonClick(SQUARE))
//      Serial.print(F("\r\nSquare"));
//
//    if (PS3.getButtonClick(UP)) {
//      Serial.print(F("\r\nUp"));
//      PS3.setLedOff();
//      PS3.setLedOn(LED4);
//    }
//    if (PS3.getButtonClick(RIGHT)) {
//      Serial.print(F("\r\nRight"));
//      PS3.setLedOff();
//      PS3.setLedOn(LED1);
//    }
//    if (PS3.getButtonClick(DOWN)) {
//      Serial.print(F("\r\nDown"));
//      PS3.setLedOff();
//      PS3.setLedOn(LED2);
//    }
//    if (PS3.getButtonClick(LEFT)) {
//      Serial.print(F("\r\nLeft"));
//      PS3.setLedOff();
//      PS3.setLedOn(LED3);
//    }
//    if (PS3.getButtonClick(L1))
//      Serial.print(F("\r\nL1"));
//    if (PS3.getButtonClick(L3))
//      Serial.print(F("\r\nL3"));
//    if (PS3.getButtonClick(R1))
//      Serial.print(F("\r\nR1"));
//    if (PS3.getButtonClick(R3))
//      Serial.print(F("\r\nR3"));
//    if (PS3.getButtonClick(SELECT)) {
//      Serial.print(F("\r\nSelect - "));
//      PS3.printStatusString();
//    }
//    if (PS3.getButtonClick(START)) {
//      Serial.print(F("\r\nStart"));
//    }
  delay(50);
}

void sendFrameB(uint8_t cmd, uint8_t data)
{
    uint8_t BUFFER[8];
    BUFFER[0] = START_FLAG;
    BUFFER[1] = CONTROLLER_ADDR;
    BUFFER[2] = DRONE_ADDR;
    BUFFER[3] = cmd;
    BUFFER[4] = 1;
    BUFFER[5] = data;
    BUFFER[6] = getChecksum(BUFFER, 8);
    BUFFER[7] = STOP_FLAG;
    Serial.write(BUFFER, 8);
}

uint8_t getChecksum(uint8_t * byte, size_t s)
{
    uint8_t sum = 0;
    
    while(s-- > 0)
        {
            sum += *(byte++);
    }
    return sum;
}
